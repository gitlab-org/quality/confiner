# Getting Started

## Installation

- Add `confiner` to your Gemfile or `gem install confiner`

## Run confiner

Confiner comes with a binary `confiner`.

- Run `$ confiner --version`
- Create a file: `.confiner/hello_world.yml`

**.confiner/hello_world.yml**

```yaml
- name: Hello, world
  plugin:
    name: debug
    args:
      arg1: Hello, world!
  actions:
    - action1
```

- Run `$ confiner`

### Additional information

- [CLI documentation](cli.md)
- [Rules documentation](rules.md)
