# Rules

Rules must ...

- Be defined in a [YAML](https://yaml.org/) file with a `.yml` or `.yaml` extension
- Be an Array
- Have a name
- Define a plugin to use
- Specify at least one action to run

## Rule Properties

Given a rule:

```yaml
- name: Rule
  plugin:
    name: debug
    args:
      arg1: foo
  actions:
    - action1
```

### `name` | **String**

The name of the rule. The name is used to discern which rule is running during logging.

### `plugin` | **Hash**

| | | |
| ------ | -------- | --- |
| `name` | `String` | Name of the plugin to execute |
| `args` | `Hash`   | Arguments to pass to the plugin |

#### `name` | **String**

Name of the plugin to execute. This name is translated to PascalCase from snake_case.

Given ...

| Given plugin name | Specify   |
| ----------------- | --------- |
| `MyPlugin`          | `my_plugin` |
| `Test`              | `test`      |

#### `args` | **Hash**

Key-value pairs of what arguments are passed to the plugin.

> See [plugin arguments](plugins.md#arguments).

### `actions` | **Array[String]**

Array of what actions to be called from the plugin.

> See [plugin actions](plugins.md#actions).

