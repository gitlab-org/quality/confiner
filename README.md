# Confiner

con·fine·ment /kənˈfīnmənt/
**noun**

The action of confining or state of being confined.

## [Get Started](doc/getting_started.md)

## What is it?

Confiner is a rule-based confinement tool.

### Examples

1. If a test fails 3 times, send an email to somebody
2. If a test passes 3 times, send another email
3. If a test failed 4 times consecutively, file a Merge Request to quarantine the test
4. If a test passes 4 times consecutively given an environment, file a Merge Request to de-quarantine the test for that given environment

## [Plugins](doc/plugins.md)
