# frozen_string_literal: true

require 'spec_helper'

module Confiner
  RSpec.describe Cli do
    describe '#new' do
      it 'outputs usage and exits when --help is specified', :aggregate_failures do
        expect { described_class.new('--help') }.to output(/Usage/).to_stdout.and raise_error(SystemExit) do |e|
          expect(e.status).to eq(0)
        end
      end

      it 'ouputs version when --version is specified' do
        expect { described_class.new('--version') }.to output(/version #{}/).to_stdout.and raise_error(SystemExit) do |e|
          expect(e.status).to eq(0)
        end
      end

      context 'logging' do
        before do
          # change to the directory with default rules so Confiner doesn't fail prematurely
          Dir.chdir('spec/fixtures/cwd')
        end

        after do
          Dir.chdir('../../..')
        end

        it 'outputs to stdout by default' do
          expect { cli }.to output(/default_rules.yml/).to_stdout
        end

        context 'with environment variables as arguments' do
          before do
            ENV['ENV_TEST'] = 'it works'
          end

          after do
            ENV['ENV_TEST'] = nil
          end

          it 'does not log the value of the env variable', :aggregate_failures do
            expect do
              cli('-r environment_variables.yml').run
            end.to output(/RULE.*\$ENV_TEST.*/).to_stdout
          end

          it 'logs the value during the action' do
            expect do
              cli('-r environment_variables.yml').run
            end.to output(/DEBUG.*it works.*/).to_stdout
          end
        end

        context 'when -o is specified' do
          before do
            cli('-o confiner.log')
          end

          after do
            File.unlink('confiner.log') if File.exist?('confiner.log')
          end

          it 'outputs to confiner.log' do
            expect(File).to exist('confiner.log')
          end
        end
      end

      context 'when -r rule is specified' do
        context 'when rule is a directory' do
          it 'loads two rules' do
            expect(cli('-r spec/fixtures/valid').instance_variable_get(:@rules).size).to eq(2)
          end
        end

        context 'when rule is a valid yaml file' do
          it 'should load the rules' do
            expect(cli('-r spec/fixtures/valid/rules.yml').instance_variable_get(:@rules)).not_to be_empty
          end
        end

        context 'when rule is not a valid file' do
          it 'raises an error of an invalid rules file' do
            expect { cli('-r spec/fixtures/invalid/invalid.yml') }.to raise_error(/must be an array/)
          end

          it 'raises an error if file is not yaml' do
            expect { cli('-r spec/fixtures/invalid/file.txt') }.to raise_error(/not a YAML file/)
          end

          context 'when rules have an invalid action' do
            it 'raises an error' do
              expect { cli('-r spec/fixtures/invalid/invalid_action.yml') }.to raise_error(/has no action/)
            end
          end

          context 'when rules have an invalid plugin' do
            it 'raises an error' do
              expect { cli('-r spec/fixtures/invalid/invalid_plugin.yml') }.to raise_error(/does not have plugin/)
            end
          end
        end
      end

      context 'when -r is not specified' do
        context 'when .confiner exists' do
          before do
            Dir.chdir('spec/fixtures/cwd')
          end

          after do
            Dir.chdir('../../..')
          end

          it 'loads .confiner/default_rules.yml' do
            expect(cli.instance_variable_get(:@rules_files)).to include(/default_rules.yml/)
          end
        end

        context 'when .confiner does not exist' do
          it 'raises an error because there are no rules to run' do
            expect { cli }.to raise_error(/No rules to run/)
          end
        end
      end
    end

    describe '#run' do
      let(:plugin) { instance_spy('plugin') }

      before do
        stub_const('Confiner::Plugins::Debug', plugin)
      end

      context 'with rules' do
        before do
          cli('-r spec/fixtures/valid/rules.yml').run
        end

        it 'calls Debug#initialize' do
          expect(plugin).to have_received(:new).with(Plugin::DEFAULT_PLUGIN_ARGS, arg1: 'debug1', arg2: 'debug2')
        end

        it 'calls Debug#run' do
          expect(plugin).to have_received(:run)
        end
      end
    end

    private

    def cli(args = '')
      described_class.new(args)
    end
  end
end
