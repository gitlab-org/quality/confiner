# frozen_string_literal: true

module Confiner
  module Logger
    # Log something with a specific level
    def log(level, message, indentation = 1)
      color = "\e[0;35m" # default purple
      color = "\e[0;33m" if %i[warn warning].include?(level) # yellow
      color = "\e[0;31m" if %i[err error fatal].include?(level) # red

      raise ArgumentError, 'Level must be less than 12 characters' if level.size > 12

      output = "(#{Time.now.strftime('%F %H:%M:%S')})\t#{color}#{level.to_s.upcase}#{' ' * (12 - level.size)}\e[m#{"\t" * indentation}#{message}"

      Logger.log_to.puts(output)
    end

    # Where to output the log
    def self.log_to
      @out_file
    end

    def self.log_to=(file)
      file = File.open(file.strip, 'a+t') if file.is_a?(String)

      @out_file = file
    end

    def run(action)
      log :plugin, "#{self.class}##{action}"
      super
      log :plugin, "#{self.class}##{action} Done"
    end
  end
end
